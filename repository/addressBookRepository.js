var mongoose = require('mongoose')
var Schema = mongoose.Schema

var addressBookSchema = new Schema({
    name: String,
    address: String,
    email: String,
    phoneNo: String
})

var AddressBookRepository = mongoose.model('Contact', addressBookSchema)

module.exports = AddressBookRepository