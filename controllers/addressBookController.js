var AddressBookService = require('../services/addressBookService')
var addressBookService = new AddressBookService()

module.exports = {

    loadMainPage: function(request, response) {
        var contacts = addressBookService.findAll((results) => {
            response.render('contacts/contacts', { title: 'Contacts', list: results });
        })
    },

    newContactPage: function(request, response) {
        response.render('contacts/contact-form', { title: 'New Contact', contact: { id: '', name: '', address: '', email: '', phoneNo: '' } })
    },

    editContactPage: function(request, response) {
        var contactId = request.params.id
        addressBookService.findById(contactId, (contact) => {
            response.render('contacts/contact-form', { title: 'Edit Contact', contact: contact })
        })
    },

    saveContact: function(request, response) {
        var contact = request.body
        if (contact.id === '') {
            addressBookService.create(contact, () => {
                response.redirect('/contacts')
            })
        } else {
            addressBookService.update(contact, () => {
                response.redirect('/contacts')
            })
        }
    },

    deleteContact: function(request, response) {
        var id = request.params.id
        addressBookService.delete(id, () => {
            response.redirect('/contacts')
        })
    }
}