var express = require('express');
var addressBookController = require('../controllers/addressBookController')

var router = express.Router();

/* GET contacts listing. */
router.get('/', function(req, res, next) {
    addressBookController.loadMainPage(req, res)
})

router.get('/new', function(req, res, next) {
    addressBookController.newContactPage(req, res)
})

router.get('/:id', function(req, res, next) {
    addressBookController.editContactPage(req, res)
})

router.get('/delete/:id', function(req, res, next) {
    addressBookController.deleteContact(req, res)
})

router.post('/save', function(req, res, next) {
    addressBookController.saveContact(req, res)
})

module.exports = router;