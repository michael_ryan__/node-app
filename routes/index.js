var express = require('express');
var request = require('request')
var jwt = require('express-jwt')
var fs = require('fs')

var jwtSecret = fs.readFileSync('./safesat.pub')

var router = express.Router();

function isAuthenticated(req, res, next) {
    if (req.session.user) {
        next()
    } else {
        res.redirect('/login')
    }
}

router.get('/login', function(req, res, next) {
    res.render('index', { title: 'Please Login' });
});

router.get('/logout', function(req, res, next) {
    req.session.user = null
    res.redirect('/login')
});

router.get('/', isAuthenticated, function(req, res, next) {
    console.log(req.user)
    res.render('index', { title: 'Express Already Logged In : ' + req.session.user.user });
});

router.get('/handle_oauth2_callback', function(req, res) {
    //TODO Save Token to session. Then GET principal in OAuth Server and also save to session or cookie.
    //TODO Add custom middleware for checking if principal is in the session or cookie. 

    request.get('http://localhost:8071/user?access_token=' + req.query.access_token, function(err, res1, body) {
        var oAuthBearer = JSON.parse(body)
        req.session.user = { user: oAuthBearer.name, authenticated: oAuthBearer.authenticated, authorities: oAuthBearer.authorities, token: req.query.access_token }
        res.redirect('/')
    })

})

module.exports = router;