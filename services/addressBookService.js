var AddressBookRepository = require('../repository/addressBookRepository')
var request = require('request')

module.exports = class AddressBookService {

    create(newContact, render) {
        AddressBookRepository.create(newContact, (err, result) => {
            render()
        })
    }

    update(contact, render) {
        AddressBookRepository.findByIdAndUpdate(contact.id, contact, (err, result) => {
            render()
        })
    }

    delete(id, render) {
        AddressBookRepository.findByIdAndRemove({ _id: id }, (err, result) => {
            render()
        })
    }

    findAll(render) {
        AddressBookRepository.find((err, results) => {
            render(results)
        })
    }

    findById(id, render) {
        AddressBookRepository.findById(id, (err, contact) => {
            render(contact)
        })
    }

}